﻿using UnityEngine;

namespace Utils
{
    public static class CollectionExt
    {
        public static Vector2 XY(this Vector3 vector3)
        {
            return new Vector2(vector3.x,vector3.y);
        }
    }
}