﻿using System;
using Unity.Mathematics;
using UnityEngine;
using Utils;

public class DragAndDrop : MonoBehaviour
{
        private Camera _camera;
        private Vector2 _mousePos;
        private Vector2 _curVelocity;
        private Rigidbody2D _rb;

        private Vector3 GetViewportPos()
        {
                Vector3 view = _camera.ScreenToViewportPoint(Input.mousePosition);
                view.x = math.clamp(view.x, 0f, 1f);
                view.y = math.clamp(view.y, 0f, 1f);
                return view;
        }
        private void Start()
        {
                _rb = GetComponent<Rigidbody2D>();
                _camera = Camera.main;
        }
        
        private void OnMouseDrag()
        {
                Vector3 curPos = _camera.ViewportToWorldPoint(GetViewportPos());
                _curVelocity = (_camera.ScreenToWorldPoint(Input.mousePosition).XY() - _mousePos) / Time.deltaTime;
                _mousePos = curPos;
                _rb.MovePosition(_mousePos);     
        }

        private void OnMouseUp()
        {
                _rb.velocity = _curVelocity;
        }
}