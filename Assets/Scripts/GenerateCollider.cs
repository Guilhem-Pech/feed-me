﻿using System;
using UnityEngine;

namespace UnityTemplateProjects
{
    public class GenerateCollider : MonoBehaviour
    {
        [SerializeField] private Camera cam;
        [SerializeField] private PhysicsMaterial2D _material2D;
        private EdgeCollider2D _upperEdge;
        private EdgeCollider2D _lowerEdge;
        private EdgeCollider2D _leftEdge;
        private EdgeCollider2D _rightEdge;
        private Resolution _res;
        private GameObject _t;


        private void OnDestroy()
        {
            #if  UNITY_EDITOR
                DestroyImmediate(_upperEdge);
                DestroyImmediate(_lowerEdge);
                DestroyImmediate(_rightEdge);
                DestroyImmediate(_leftEdge);     
            #else
                Destroy(_upperEdge);
                Destroy(_lowerEdge);
                Destroy(_rightEdge);
                Destroy(_leftEdge);
            #endif
        }

        private void Start()
        {
           
            cam = Camera.main;
            _upperEdge = new GameObject("upperEdge").AddComponent<EdgeCollider2D>();
            _upperEdge.transform.parent = transform;
            _lowerEdge = new GameObject("lowerEdge").AddComponent<EdgeCollider2D>();
            _lowerEdge.transform.parent = transform;
            _leftEdge = new GameObject("leftEdge").AddComponent<EdgeCollider2D>();
            _leftEdge.transform.parent = transform;
            _rightEdge = new GameObject("rightEdge").AddComponent<EdgeCollider2D>();
            _rightEdge.transform.parent = transform;
            
            _upperEdge.sharedMaterial = _material2D;
            _lowerEdge.sharedMaterial = _material2D;
            _leftEdge.sharedMaterial = _material2D;
            _rightEdge.sharedMaterial = _material2D;

            GenerateCollidersAcrossScreen();
            _res = Screen.currentResolution;
        }

        private void Update()
        {
            if (_res.height != Screen.height || _res.width != Screen.width)
            {
                GenerateCollidersAcrossScreen();
                _res = Screen.currentResolution;
            }
        }

        private void GenerateCollidersAcrossScreen()
        {
            Vector2 lDCorner = cam.ViewportToWorldPoint(new Vector3(0, 0f, cam.nearClipPlane));
            Vector2 rUCorner = cam.ViewportToWorldPoint(new Vector3(1f, 1f, cam.nearClipPlane));
            Vector2[] colliderpoints;
 
            
            colliderpoints = _upperEdge.points;
            colliderpoints[0] = new Vector2(lDCorner.x, rUCorner.y);
            colliderpoints[1] = new Vector2(rUCorner.x, rUCorner.y);
            _upperEdge.points = colliderpoints;
            
            colliderpoints = _lowerEdge.points;
            colliderpoints[0] = new Vector2(lDCorner.x, lDCorner.y);
            colliderpoints[1] = new Vector2(rUCorner.x, lDCorner.y);
            _lowerEdge.points = colliderpoints;
 
           
            colliderpoints = _leftEdge.points;
            colliderpoints[0] = new Vector2(lDCorner.x, lDCorner.y);
            colliderpoints[1] = new Vector2(lDCorner.x, rUCorner.y);
            _leftEdge.points = colliderpoints;
 
            
            colliderpoints = _rightEdge.points;
            colliderpoints[0] = new Vector2(rUCorner.x, rUCorner.y);
            colliderpoints[1] = new Vector2(rUCorner.x, lDCorner.y);
            _rightEdge.points = colliderpoints;
        }
    }
}